/* eslint-disable react/jsx-indent */
/* eslint-disable indent */
import { useDispatch, useSelector } from 'react-redux';

import { useEffect } from 'react';

import Categories from 'components/Categories';
import PizzaLoader from 'components/Loaders';
import PizzaBlock from 'components/PizzaBlock';
import Sort from 'components/Sort';

import { setCategory, setSortBy } from 'redux/actions/filters';
import { fetchPizzas } from 'redux/actions/pizzas';
import { setPizzasToCart } from 'redux/actions/cart';

const Main = () => {
  const dispatch = useDispatch();
  const { items, isLoaded } = useSelector(({ pizzas }) => {
    return pizzas;
  });
  const { sortBy, category } = useSelector(({ filters }) => filters);

  useEffect(() => {
    dispatch(fetchPizzas(sortBy, category));
  }, [sortBy, category, dispatch]);

  const onSelectType = (type) => {
    dispatch(setSortBy(type));
  };

  const onSelectCategory = (categoryIndex) => {
    dispatch(setCategory(categoryIndex));
  };

  const addPizzaToCart = (obj) => {
    dispatch(setPizzasToCart(obj));
  };

  const categories = [
    'Все',
    'Мясные',
    'Вегетарианская',
    'Гриль',
    'Острые',
    'Закрытые',
  ];

  const sortItems = [
    { name: 'популярности', type: 'popular' },
    { name: 'цене', type: 'price' },
    { name: 'алфавиту', type: 'name' },
  ];

  return (
    <div className="content">
      <div className="container">
        <div className="content__top">
          <Categories
            items={categories}
            onSelectCategory={onSelectCategory}
            activeCategory={category}
          />
          <Sort items={sortItems} onSelectType={onSelectType} sortBy={sortBy} />
        </div>
        <h2 className="content__title">Все пиццы</h2>
        <div className="content__items">
          {isLoaded
            ? items.map((item) => (
                <PizzaBlock
                  key={item.id}
                  addPizzaToCart={addPizzaToCart}
                  {...item}
                />
              ))
            : Array(12)
                .fill(0)
                // eslint-disable-next-line react/no-array-index-key
                .map((_, index) => <PizzaLoader key={index} />)}
        </div>
      </div>
    </div>
  );
};

export default Main;

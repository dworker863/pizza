import CartEmpty from 'components/CartEmpty';
import CartFull from 'components/CartFull';
import { useSelector } from 'react-redux';

const Cart = () => {
  const { items, totalPizzaCount, totalPrice } = useSelector(
    ({ cart }) => cart,
  );
  return (
    <div className="content">
      {totalPrice === 0 ? (
        <CartEmpty />
      ) : (
        <CartFull
          items={items}
          totalPizzaCount={totalPizzaCount}
          totalPrice={totalPrice}
        />
      )}
    </div>
  );
};

export default Cart;

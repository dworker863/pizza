import classNames from 'classnames';

const Categories = ({ items, activeCategory, onSelectCategory }) => {
  return (
    <div className="categories">
      <ul>
        {items.map((item, index) => (
          // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
          <li
            onClick={() => onSelectCategory(index)}
            onKeyPress={() => onSelectCategory(index)}
            className={classNames({
              active: activeCategory === index,
            })}
            // eslint-disable-next-line react/no-array-index-key
            key={`${item}-${index}`}
          >
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Categories;

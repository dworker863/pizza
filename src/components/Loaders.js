import ContentLoader from 'react-content-loader';

const PizzaLoader = (props) => (
  <ContentLoader
    speed={2}
    width={280}
    height={457}
    viewBox="0 0 280 457"
    backgroundColor="#e4e4dd"
    foregroundColor="#fafafa"
    {...props}
  >
    <rect x="502" y="195" rx="3" ry="3" width="88" height="6" />
    <circle cx="135" cy="132" r="120" />
    <rect x="0" y="265" rx="3" ry="3" width="280" height="24" />
    <rect x="0" y="305" rx="10" ry="10" width="280" height="84" />
    <rect x="0" y="413" rx="4" ry="4" width="89" height="27" />
    <rect x="129" y="403" rx="20" ry="20" width="151" height="44" />
  </ContentLoader>
);

export default PizzaLoader;

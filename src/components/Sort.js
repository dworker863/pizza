import { useEffect, useRef, useState, memo } from 'react';

const Sort = memo(({ items, onSelectType, sortBy }) => {
  const [popupVisibility, setPopupVisibility] = useState(false);
  const ref = useRef();

  const setSelected = (type) => {
    onSelectType(type);
    setPopupVisibility(false);
  };

  const togglePopup = () => {
    setPopupVisibility(!popupVisibility);
  };

  const handleClick = (event) => {
    const path = event.path || (event.composedPath && event.composedPath());

    if (!path.includes(ref.current)) {
      setPopupVisibility(false);
    }
  };

  useEffect(() => {
    return window.addEventListener('click', handleClick);
  }, []);

  return (
    <div ref={ref} className="sort">
      <div
        // eslint-disable-next-line jsx-a11y/role-has-required-aria-props
        role="option"
        tabIndex={0}
        onClick={togglePopup}
        onKeyPress={togglePopup}
        className="sort__label"
      >
        <svg
          className={!popupVisibility && 'triangle'}
          width="10"
          height="6"
          viewBox="0 0 10 6"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M10 5C10 5.16927 9.93815 5.31576 9.81445 5.43945C9.69075 5.56315 9.54427 5.625 9.375 5.625H0.625C0.455729 5.625 0.309245 5.56315 0.185547 5.43945C0.061849 5.31576 0 5.16927 0 5C0 4.83073 0.061849 4.68424 0.185547 4.56055L4.56055 0.185547C4.68424 0.061849 4.83073 0 5 0C5.16927 0 5.31576 0.061849 5.43945 0.185547L9.81445 4.56055C9.93815 4.68424 10 4.83073 10 5Z"
            fill="#2C2C2C"
          />
        </svg>
        <b>Сортировка по:</b>
        <span>{items.find(({ type }) => type === sortBy).name}</span>
      </div>
      {popupVisibility && (
        <div className="sort__popup">
          <ul>
            {items.map(({ type, name }) => (
              // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
              <li
                onClick={() => setSelected(type)}
                onKeyPress={() => setSelected(type)}
                className={sortBy === type && 'active'}
                // eslint-disable-next-line react/no-array-index-key
                key={type}
              >
                {name}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
});

Sort.displayName = 'Sort';

export default Sort;

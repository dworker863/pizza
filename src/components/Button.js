import classNames from 'classnames';

const Button = ({
  clickHandler,
  outline,
  cart,
  add,
  children,
  circle,
  minus,
}) => {
  return (
    <div>
      <button
        onClick={clickHandler}
        type="button"
        className={classNames('button', {
          'button--outline': outline,
          'button--add': add,
          'button--cart': cart,
          'button--circle': circle,
          'cart__item-count-minus': minus,
        })}
      >
        {children}
      </button>
    </div>
  );
};

export default Button;

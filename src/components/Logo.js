import logo from '../assets/img/pizza-logo.svg';

const Logo = () => {
  return (
    <>
      <img width="38" src={logo} alt="Pizza logo" />
      <div>
        <h1>React Pizza</h1>
        <p>самая вкусная пицца во вселенной</p>
      </div>
    </>
  );
};

export default Logo;

import { getTotalItemsCount, getTotalPrice } from 'utils/utils';

/* eslint-disable no-case-declarations */
const initialState = {
  items: {},
  totalPrice: 0,
  totalPizzaCount: 0,
};

const cart = (state = initialState, { type, payload }) => {
  switch (type) {
    case 'SET_PIZZA_TO_CART':
      const newState = {
        ...state,
        items: {
          ...state.items,
          [payload.id]: !state.items[payload.id]
            ? [payload]
            : [...state.items[payload.id], payload],
        },
      };

      const totalPizzaCount = getTotalItemsCount(newState.items);

      const totalPrice = getTotalPrice(totalPizzaCount);

      return {
        ...newState,
        totalPizzaCount: totalPizzaCount.length,
        totalPrice,
      };

    case 'CLEAR_CART':
      return {
        items: {},
        totalPrice: 0,
        totalPizzaCount: 0,
      };

    case 'REMOVE_ADDED_PIZZAS':
      const itemsForRemovePizzaType = { ...state.items };

      delete itemsForRemovePizzaType[payload];

      return {
        items: itemsForRemovePizzaType,
        totalPizzaCount: getTotalItemsCount(itemsForRemovePizzaType).length,
        totalPrice: getTotalPrice(getTotalItemsCount(itemsForRemovePizzaType)),
      };

    case 'ADD_PIZZA':
      const itemsForAddPizza = {
        ...state.items,
        [payload]: [...state.items[payload], { ...state.items[payload][0] }],
      };

      return {
        items: itemsForAddPizza,
        totalPizzaCount: getTotalItemsCount(itemsForAddPizza).length,
        totalPrice: getTotalPrice(getTotalItemsCount(itemsForAddPizza)),
      };

    case 'REMOVE_PIZZA':
      const itemsForRemovePizza = {
        ...state.items,
        [payload]: state.items[payload].slice(1),
      };

      return {
        items: itemsForRemovePizza,
        totalPizzaCount: getTotalItemsCount(itemsForRemovePizza).length,
        totalPrice: getTotalPrice(getTotalItemsCount(itemsForRemovePizza)),
      };

    default:
      return state;
  }
};

export default cart;

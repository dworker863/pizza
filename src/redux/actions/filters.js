export const setSortBy = (sort) => ({
  type: 'SET_SORT_BY',
  payload: sort,
});

export const setCategory = (catIndex) => ({
  type: 'SET_CATEGORY',
  payload: catIndex,
});

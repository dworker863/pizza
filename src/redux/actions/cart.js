export const setPizzasToCart = (pizza) => ({
  type: 'SET_PIZZA_TO_CART',
  payload: pizza,
});

export const clearCart = () => ({
  type: 'CLEAR_CART',
});

export const removeAddedPizzas = (id) => ({
  type: 'REMOVE_ADDED_PIZZAS',
  payload: id,
});

export const addPizza = (id) => ({
  type: 'ADD_PIZZA',
  payload: id,
});

export const removePizza = (id) => ({
  type: 'REMOVE_PIZZA',
  payload: id,
});

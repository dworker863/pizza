import axios from 'axios';

export const setPizzas = (items) => ({
  type: 'SET_PIZZAS',
  payload: items,
});

export const setLoaded = (payload) => ({
  type: 'SET_LOADED',
  payload,
});

export const fetchPizzas = (sortBy, category) => (dispatch) => {
  dispatch(setLoaded(false));
  axios
    .get(
      `http://localhost:3001/pizzas?${
        category === 0 ? '' : `category=${category}`
      }&_sort=${sortBy}&_order=${sortBy === 'alphabet' ? 'des' : 'asc'}`,
    )
    .then(({ data }) => {
      // eslint-disable-next-line no-debugger
      // debugger;
      dispatch(setPizzas(data));
    });
};

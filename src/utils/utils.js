/* eslint-disable prefer-spread */
export const getTotalItemsCount = (obj) => {
  return [].concat.apply([], Object.values(obj));
};

export const getTotalPrice = (arr) => {
  return arr.reduce((sum, item) => sum + item.price, 0);
};

import { Route } from 'react-router-dom';

import Main from 'pages/Main';
import Cart from 'pages/Cart';
import Header from './components/Header';

function App() {
  return (
    <div className="wrapper">
      <Header />
      <Route exact path="/" component={Main} />
      <Route path="/cart" component={Cart} />
    </div>
  );
}

export default App;
